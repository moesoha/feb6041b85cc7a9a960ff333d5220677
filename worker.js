addEventListener('fetch', event => event.respondWith(handleRequest(event.request)));

// Add environment variable `TGBOT_TOKEN` via Worker-Settings
async function requestTelegramBotAPI(method, payload) {
    return fetch(`https://api.telegram.org/bot${TGBOT_TOKEN}/${method}`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: !payload ? undefined : JSON.stringify(payload)
    });
}

// Add environment variable `MANAGE_TOKEN` via Worker-Settings
const ManageToken = typeof MANAGE_TOKEN !== 'undefined' ? MANAGE_TOKEN : null;
const RequestHandlers = {
    async '/tgWebhookDelete'(_, url) {
        if(!ManageToken || url.search !== `?${ManageToken}`) {
            return new Response('Access Denied', {status: 403});
        }
        const result = await requestTelegramBotAPI('deleteWebhook');
        return new Response(await result.text());
    },
    async '/tgWebhookSet'(_, url) {
        if(!ManageToken || url.search !== `?${ManageToken}`) {
            return new Response('Access Denied', {status: 403});
        }
        const result = await requestTelegramBotAPI('setWebhook', {
            url: `https://${url.host}/tgPush`,
            allowed_updates: ['message']
        });
        return new Response(await result.text());
    },
    async '/tgPush'(request) {
        if(request.method !== "POST") {
            return new Response('Bad Request', {status: 400});
        }
        const body = await request.json().catch(_ => ({}));
        const message = body.message;
        if(!message) {
            return;
        }
        if(message.new_chat_members) {
            for(const i in message.new_chat_members) {
                const u = message.new_chat_members[i];
                await requestTelegramBotAPI('banChatMember', {
                    chat_id: message.chat.id,
                    user_id: u.id,
                    until_date: Math.floor(new Date().getTime() / 1000) + 86400
                });
            }
        }
        if(message.left_chat_member || message.new_chat_members) {
            await requestTelegramBotAPI('deleteMessage', {
                chat_id: message.chat.id,
                message_id: message.message_id
            });
        }
    }
}

async function handleRequest(request) {
    const url = new URL(request.url);
    if(typeof RequestHandlers[url.pathname] !== 'function') {
        return new Response('Not Found', {status: 404});
    }
    const response = await RequestHandlers[url.pathname](request, url);
    return response || (new Response('WOW', {status: 200}));
}
